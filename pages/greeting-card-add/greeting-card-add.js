import { Router, Common } from "../../utils/common.js";
import { RemoteDataService } from '../../utils/remoteDataService.js';

Page({

  data: {
    userAvatar: "",
    petImg: "",
    masterWords: "",
    nickName: "",
    petWords: "",
    petName: "",
    petAvatar: "",
    blessing: "",
    cardBg: "",
    cardVali: {}
  },

  onLoad: function (options) {
    let that = this
    //download the bg
    that.data.cardBg = ""
    wx.showNavigationBarLoading()
    RemoteDataService.getPetCardBg({}).then(result => {
      if (result && result.code == "000") {
        let remoteCardBg = result.cardBg
        wx.downloadFile({
          url: result.cardBg,
          success: function (res) {
            console.log(res)
            if (res.statusCode === 200) {
              if (res.tempFilePath){
                that.data.cardBg = res.tempFilePath
                that.data.cardVali.cardBgOk = true
                console.log(that.data.cardBg)
              }
            }
          }
        })
        that.data.masterWords = result.masterWords
        that.data.petWords = result.petWords
        that.data.petName = result.petName
        that.data.petAvatar = result.petAvatar
        that.data.blessing = result.blessingWords
        that.setData({
          petAvatar: result.petAvatar
        })
        wx.downloadFile({
          url: that.data.petAvatar,
          success: function (res) {
            console.log(res)
            if (res.statusCode === 200) {
              if (res.tempFilePath) {
                that.data.petImg = res.tempFilePath
                console.log(that.data.petImg)
              }
            }
          }
        })
      }
      wx.hideNavigationBarLoading()
    }).catch(err => {
      wx.hideNavigationBarLoading()
    })
    wx.downloadFile({
      url: that.data.userAvatar,
      success: function (res) {
        console.log(res)
        if (res.statusCode === 200) {
          if (res.tempFilePath) {
            that.data.petImg = res.tempFilePath
            that.data.cardVali.cardBgOk = true
            console.log(that.data.cardBg)
          }
        }
      }
    })
  },
  selectImg: function(){
    let that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success(res) {
        that.data.cardVali.petImgOk = true
        that.setData({
          petImg: res.tempFilePaths[0]
        })
      }
    })
  },
  getNickName: function (e) {
    let that = this
    that.data.nickName = e.detail.value
    if (that.data.nickName) {
      that.data.cardVali.nickNameOk = true
    } else {
      that.data.cardVali.nickNameOk = false
    }
  },
  postCard: function () {
    let that = this
    console.log("that.data.petImg=", that.data.petImg)
    let cardVali = that.data.cardVali;
    // if (!cardVali.petImgOk) {
    //   Common.myToast("请上传头像", 2000);
    //   return;
    // } else 
    if (!cardVali.nickNameOk) {
      Common.myToast("请输入名字", 2000);
      return;
    } else if (!cardVali.cardBgOk) {
      Common.myToast("数据加载中", 2000);
      return;
    }
    let params = {
      petImg: that.data.petImg,
      nickName: that.data.masterWords + " " +that.data.nickName,
      petName: that.data.petWords + " " +that.data.petName,
      blessing: that.data.blessing,
      cardBg: that.data.cardBg
    }
    Router.navigateTo("../greeting-card/greeting-card", params);
  }

})