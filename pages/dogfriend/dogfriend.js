import { Router } from "../../utils/common.js";
import { RemoteDataService } from '../../utils/remoteDataService.js';

Page({

  data: {
    pet: {},
    petFriends: []
  },

  onLoad: function (options) {
    let that = this
    let params = {}
    wx.showNavigationBarLoading()
    RemoteDataService.getPetHouseList(params).then(result => {
      if (result && result.code == "000") {
        that.setData({
          pet: result.pet,
          petFriends: result.petFriends
        })
      }
      wx.hideNavigationBarLoading()
    }).catch(err => {
      wx.hideNavigationBarLoading()
    })
  },
  navToPetDet: function (e) {
    let params = {
      petId: e.currentTarget.dataset.petid
    }
    Router.navigateTo("../dogdet/dogdet", params);
  }


})